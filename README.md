ykocli - Command line front-end to YKman
========================================

Command line front-end for the Yubikey Manager (ykman).
https://www.yubico.com/support/download/yubikey-manager
Functionality is limited to the obtainment of single use oath codes.

Additional information, sample configurations and examples
can be found in the ykocli Wiki located:  https://bitbucket.org/gbcox/ykocli/wiki/Home

Licensed under GNU GPL version 3 or later

Copyright © 2022 Gerald Cox (unless explicitly stated otherwise)
