# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0▷⋅)
	printf "%s\nScript must be invoked via source command\nExiting\n"
	exit 1;;
esac


if [[ ${#MAPFILE[@]} == "32" ]] && [[ $firmware_level -lt "570" ]]; then
	printf "${CWARN}Maximum number of OATH accounts detected on Yubikey\n"
	printf "${CALERT}Exiting${RESTORE}\n"
	return 28
fi

if [[ ${#MAPFILE[@]} == "64" ]]; then
	printf "${CWARN}Maximum number of OATH accounts detected on Yubikey\n"
	printf "${CALERT}Exiting${RESTORE}\n"
	return 28
fi

shopt -s extglob

supported_methods="SCAN | TEXT"
methods="@(SCAN|TEXT|QUIT|EXIT|-H|--HELP)"

#issuer limited to 51
#account name limited to 54

source "$SRC/src-yko-figlet.sh"

until [[ $valid_method == "YES" ]]; do
	case "${method_req^^}" in
		$methods	)
		valid_method="YES";;
		* )
		printf "${CINFO}This will add a new TOTP account to the Yubikey.\n"
		printf "${CREQUEST}Select Supported Method: ${CACTION}$supported_methods${CPROMPT}\n"
		read -p "[ENTER]: " method_req
		printf "${RESTORE}\n";;
	esac
done

shopt -u extglob

case "${method_req^^}" in
	"QUIT"|"EXIT"	)
	printf "${CALERT}%s${RESTORE}\n" "Exiting.  You entered $method_req."
	return 125;;
	"-H"|"--HELP"	)
	source "$SRC/src-yko-help.sh"
	return 125;;

	"SCAN"	)
	until [[ $file_found == "YES" ]]; do
		printf "${CPROMPT}"
		read -p "[ENTER] Location of barcode image file: " scan_file
		case "${scan_file^^}" in
			"QUIT"|"EXIT"	)
			printf "${CALERT}%s${RESTORE}\n" "Exiting.  You entered $scan_file."
			return 125;;
			"-H"|"--HELP"	)
			source "$SRC/src-yko-help.sh"
			return 125;;
		esac
		if [[ -f "$scan_file" ]]; then
			file_found="YES"
		else
			printf "${CALERT}File ${CHLITE}$scan_file ${CALERT}not found.\n${RESTORE}"
		fi
	done
	
	scan_string=$(zbarimg -q $scan_file)
	scan_rc=$?

	if [[ $scan_rc -ne "0" ]]; then
		printf "${CALERT}ERROR: ${CHLITE}$scan_file ${CALERT}invalid.\n"
		printf "${CALERT}EXITING.${RESTORE}\n"
		return $scan_rc
	fi

	scan_string=${scan_string//QR-Code\:otpauth:\/\/totp\//}
	scan_string=$(echo -e "${scan_string//%/\\x}")

	mapfile -d '&' -t scan_table < <(printf "$scan_string")
	
	for scan_index in "${scan_table[@]}";
	do
		if [[ "$scan_index" =~ "?" ]]; then
			account=${scan_index%%\?*}
		fi
		if [[ "$scan_index" =~ "secret=" ]]; then
			secret=${scan_index##*secret=}
		fi
		if [[ "$scan_index" =~ "issuer=" ]]; then
			issuer=${scan_index##*issuer=}
		fi
	done;;

	"TEXT"	)

	printf "${CPROMPT}"
	read -p "[ENTER] <Optional> Issuer, i.e. Google, Facebook, etc.: " issuer
	read -p "[ENTER] Name, i.e. username or e-mail address: " account
	until [[ $secret != "" ]]; do
		read -p "[ENTER] SECRET, i.e. Base32-encoded secret/key value provided by the server: " secret
	done

	printf "${RESTORE}"

	if [[ "${secret}" =~ [^a-zA-Z2-7=] ]]; then
		printf "${CALERT}SECRET ${CHLITE}%s ${RED}invalid.${RESTORE}\n" $secret
		printf "${CALERT}Exiting.${RESTORE}\n"
		return 22
	fi

esac

	if [[ $issuer != "" ]]; then
		demarc=":"
	else
		demarc=""
	fi

	namestring="$issuer$demarc$account"

if [[ ${MAPFILE[@]} =~ " $namestring" ]]; then
	effective_action="REPLACE"
	action_msg="replaced"
	printf "${CINFO}Account ${CHLITE}$namestring ${CINFO}already exists.\n"
else
	effective_action="ADD"
	action_msg="added"
fi

printf "\n${CACTION}$effective_action\n"
printf "${CARROW}===> ${CHLITE}$namestring ${CARROW}<=== ${CINFO}\n"
printf "${CINFO}You must reply ${CHLITE}YES${CINFO} to confirm.${CPROMPT}\n"
read -p "[ENTER] YES to confirm: " add_confirmation
if [[ $add_confirmation == "YES" ]]; then
	add_error=$(ykman -d $key_serial oath accounts add --force "$namestring" "$secret" 2>&1)
	ykman_rc=$?
	add_error=${add_error##*Error: }
	if [[ $ykman_rc -ne "0" ]]; then
		printf "${CHLITE}$add_error\n"
		printf "${CALERT}Exiting.${RESTORE}\n"
		return 74
	fi
else
	printf "${CINFO}Valid confirmation not received.  ${CALERT}EXITING.${RESTORE}\n"
	return 125
fi

printf "${CINFO}Account ${CHLITE}$namestring ${CINFO}$action_msg.\n"

return
