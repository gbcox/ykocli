#! /usr/bin/bash
#
# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SRC="/usr/libexec/ykocli"
source "$SRC/src-yko-set-colors.sh"

konsole_filename="ykocli.profile"
desktop_filename="ykocli.desktop"
autostart_file="${HOME}/.config/autostart/${desktop_filename}"
konsole_file="${HOME}/.local/share/konsole/${konsole_filename}"
desktop_file="${HOME}/.local/share/applications/${desktop_filename}"
change_me="\/directory_name"

exec_dirname=$(dirname "$0")
exec_path=$(cd "$exec_dirname" && pwd)

help() {
printf "${LGREEN}Integrate ykocli for KDE Konsole Background Mode\n\n"
printf "${LRED}${BLINK}REMINDER: ${RESTORE}${WHITE}Background mode can only be used with KDE Plasma\n"
printf "and Konsole since it uses features and functionality\n"
printf "unique to KDE\n\n"
printf "${LBLUE}Invoke from your home directory by entering:\n"
printf "${LYELLOW}bash /usr/share/doc/ykocli/ykocli-konsole-background.sh"
printf "${RESTORE}\n\n"
printf "Usage: -i | --install    -- install bkgnd files\n"
printf "       -u | --uninstall  -- uninstall bkgnd files\n"
printf "       -h | --help       -- show usage\n"
}

install() {
sed -e "s|@exec_path|${exec_path}|g" \
<"${exec_path}/${desktop_filename}" \
>"${desktop_file}"
printf "${LCYAN}Created file: ${desktop_file}\n"
sed -e "s|@exec_path|${exec_path}|g" \
<"${exec_path}/${konsole_filename}" \
>"${konsole_file}"
printf "Created file: ${konsole_file}\n"
sed -e "s|@exec_path|${exec_path}|g" \
<"${exec_path}/${desktop_filename}" \
>"${autostart_file}"
printf "Created file: ${autostart_file}\n"
printf "${RESTORE}\n"
}

uninstall() {
rm "${desktop_file}"
printf "${LCYAN}Removed: ${desktop_file}\n"
rm "${konsole_file}"
printf "Removed: ${konsole_file}\n"
rm "${autostart_file}"
printf "Removed: ${autostart_file}\n"
printf "${RESTORE}\n"
}

while [[ "$#" -gt 0 ]]; do

clear -x

case $1 in
	-i | --install)
	install
	exit 0;;
	-u | --uninstall)
	uninstall
	exit 0;;
	-h | --help)
	help
	exit 0;;
	* )
	printf "${LRED}Unknown parameter passed: $1\n"
	printf "${RESTORE}\n"
	help
	exit 1;;
esac

shift

done

clear -x
help
