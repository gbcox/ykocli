# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0	)
		printf "%s\nScript must be invoked via source command\nExiting\n"
		exit;;
esac

printf "%s\n" "This is ykocli v$VERSION" \
" " \
"USAGE: ykocli [action] [search_string]" \
" " \
"       ykocli TOTP   [search_string] - Obtain TOTP Token" \
"       ykocli RENAME [search_string] - RENAME Account" \
"       ykocli DELETE [search_string] - DELETE Account" \
"       ykocli BKGND  - TOTP Konsole Background Mode (KDE Plasma Only)" \
"       ykocli ADD    - ADD Account" \
"       ykocli QUIT   - Immediately exit script" \
"       ykocli EXIT   - Immediately exit script" \
"       ykocli -H     - Display HELP" \
"       ykocli --HELP - Display HELP" \
" " \
"USAGE NOTES:" \
" " \
"       [search_string]" \
"              ykocli will only display OATH entries that match the" \
"              search_string entered on the command line." \
" " \
"       BKGND" \
"              Konsole running under KDE Plasma ONLY." \
"              Invoke by using Konsole background mode, i.e." \
"              konsole --background-mode -e ykocli bkgnd or with an" \
"              optional konsole profile, i.e." \
"              konsole --background-mode --profile xyz -e ykocli bkgnd" \
"              This command can be entered using krunner, or as part of" \
"              an autostart routine." \
"              The Konsole window will start in the background.  Press" \
"              the default hotkey CRTL+SHIFT+F12 to toggle the window" \
"              between ACTIVE and BACKGROUND." \
"              Examples for desktop and profile files can be found" \
"              in the /usr/share/doc/ykocli/examples directory." \
"              Alternatively, you can invoke the following command" \
"              from your home directory:" \
"              bash /usr/share/doc/ykocli/examples/ykocli-konsole-bkgnd.sh" \
" "
return
