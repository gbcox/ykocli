# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

valid_index="NO"

i=0

source "$SRC/src-yko-figlet.sh"

mapfile -t device_info < <(ykman list)

dev_count=${#device_info[@]}

if [[ $dev_count == "0" ]]; then
	printf "${CERROR}%sValid YubiKey needs to be properly detected\n"
	printf "${CERROR}%sPlease verify YubiKey is inserted\n"
	printf "${CERROR}%sExiting.\n"
	printf "${RESTORE}\n"
	return 6
fi

if [ $dev_count -gt 1 ]; then
	printf "${CINFO}Mulitple Yubikeys detected\n"
	printf "${CREQUEST}Choose an entry below:\n\n"

	while [[ "$i" -lt "$dev_count" ]]; do

		fullstring=${device_info[$i]}
		
		if [[ $(( i % 2 )) == 0 ]]
		then
			printf "${CTBLNA}index=%-2s ====> ${CTBLNA}$fullstring\n" $i
		else
			printf "${CTBLNB}index=%-2s ====> ${CTBLNB}$fullstring\n" $i
		fi
		
		let i++
	
	done

	until [[ $valid_index == "YES" ]]; do

	printf "\n${CREQUEST}Enter desired numeric index value ${CHLITE}$action_msg${CREQUEST}and press ${CPROMPT}\n"
	read -p "[ENTER]:  " device_index

	case "${device_index^^}" in
		"QUIT"|"EXIT"   )
		 printf "${CALERT}%s${RESTORE}\n" "Exiting.  You entered $device_index."
		exit;;
	esac

	if  ! [[ ${device_index} =~ $ck_index ]]; then
		valid_index="NO"
		printf "${CWARN}Invalid entry\n"
	elif [[ "$dev_count" -gt "$device_index" ]]; then
		valid_index="YES"
	fi

	done

else
	device_index=0
fi

return
