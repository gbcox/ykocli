# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0	)
	printf "%s\nScript must be invoked via source command\nExiting\n"
	exit 1;;
esac

if [[ $from_src_yko_add == "YES" ]] || [[ $touch_em == "YES" ]]; then
	:
else
	fullstring=${MAPFILE[$input_index]}
	namestring=${MAPFILE[$input_index]%% *}
fi

if [[ $fullstring =~ "[Requires Touch]" ]]; then
	rq_touch="YES"
fi

authcode=$(ykman -d $key_serial oath accounts code "$namestring")
authcode=${authcode##* }

if [[ $authcode == "" ]]; then
	printf "\n${CERROR}ykman Failure.\n"
	printf "${CALERT}Exiting.${RESTORE}\n"
	return 74
fi

if copyq copy $authcode &> /dev/null; then
	copyq_msg="is now in the clipboard"
	if [[ $WAYLAND_DISPLAY == "wayland-0" ]] && [[ $XDG_CURRENT_DESKTOP =~ "GNOME" ]]; then
		wayland_result="$(copyq clipboard)"
		if [[ $wayland_result == "" ]]; then
			copyq_msg="must be manually copied to clipboard"
			printf "\n${CALERT}CopyQ clipboard functionality not working\n"
			printf "${CINFO}CopyQ under GNOME Wayland needs to be started by\n"
			printf "${CEMPH}===> ${CHLITE}env QT_QPA_PLATFORM=xcb copyq${RESTORE}\n\n"
			printf "${CINFO}If CopyQ autostarts, you need to change the ${CHLITE}Exec=...\n"
			printf "${CINFO}line in: ${CHLITE}~/.config/autostart/copyq.desktop\n"
			printf "${CINFO}to ${CHLITE}Exec=env QT_QPA_PLATFORM=xcb copyq${RESTORE}\n"
		fi
	fi
else
	copyq_msg="must be manually copied to clipboard"
	printf "\n${CALERT}CopyQ daemon inactive, disabling clipboard functionality\n"
fi

printf "\n${CINFO}Entry ${CHLITE}$namestring ${CINFO}has been selected\n"
printf "${CINFO}TOTP token ${CHLITE}$authcode ${CINFO}$copyq_msg\n"

source "$SRC/src-yko-time.sh"

if [[ $remaining_seconds -le $REGEN ]] && [[ "$rq_touch" != "YES" ]]; then

	printf "\n\n${CALERT}TOTP token expiration was below configuration threshold\n"

	printf "\r${CLRLINE}${CINFO}Generating replacement TOTP token $countdown_message"

	authcode=$(ykman -d $key_serial oath accounts code "$namestring")
	authcode=${authcode##* }

	copyq copy $authcode &> /dev/null

	printf "\r${CINFO}Replacement TOTP token ${CHLITE}$authcode ${CINFO}$copyq_msg\n"

	source "$SRC/src-yko-time.sh"

fi

printf "${RESTORE}\n"
return
