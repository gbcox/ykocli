# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0	)
	printf "%s\nScript must be invoked via source command\nExiting\n"
	exit 1;;
esac

fullstring=${MAPFILE[$input_index]}
namestring=${MAPFILE[$input_index]%% *}

printf "\n${CINFO}DELETE ${CARROW}===> ${CHLITE}$namestring ${CARROW}<====\n"
printf "${CWARN}This action will permanently DELETE the account${RESTORE}\n"
printf "${CREQUEST}You must reply ${CHLITE}YES${CREQUEST} to confirm.${CPROMPT}\n"
read -p "[ENTER] YES to confirm: " delete_confirmation

if [[ $delete_confirmation == "YES" ]]; then
	delete_code=$(ykman -d $key_serial oath accounts delete --force "$namestring")
		printf "${CINFO}$delete_code ${CALERT}EXITING.${RESTORE}\n"
	else
		printf "${CINFO}Valid confirmation not received. ${CALERT}EXITING.${RESTORE}\n"
		return 125
	fi

return
