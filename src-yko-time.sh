# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0	)
	printf "%s\nScript must be invoked via source command\nExiting\n"
	exit 1;;
esac

cur_seconds=$(date +%S)
cur_seconds=$((10#$cur_seconds))

if [[ "$cur_seconds" -lt 30 ]]
then
	remaining_seconds=$(( 30 - cur_seconds ))
else
	remaining_seconds=$(( 60 - cur_seconds ))
fi

for (( c=$start; c<=$remaining_seconds; c++ )); do
        sleep 1
        countdown=$((remaining_seconds-c))
        if [[ "$countdown" -gt "0" ]]; then
                countdown_message="will expire in ${CHLITE}$countdown ${CINFO}seconds"
        else
                countdown_message="has expired"
        fi
        printf "\r${CLRLINE}${CINFO}TOTP token $countdown_message"
done

return
