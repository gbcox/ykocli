# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0	)
	printf "%s\nScript must be invoked via source command\nExiting\n"
	exit 1;;
esac

valid_index="NO"

i=0

count=${#MAPFILE[@]}
source "$SRC/src-yko-figlet.sh"

if [ $count -gt 1 ]; then
	printf "${CREQUEST}Choose an entry below:\n"

	while [[ "$i" -lt "$count" ]]; do

		fullstring=${MAPFILE[$i]}i
		namestring=${MAPFILE[$i]%% *}
		
		if [[ $(( i % 2 )) == 0 ]]
		then
			printf "${CTBLNA}index=%-2s ====> ${CTBLNA}$namestring\n" $i
		else
			printf "${CTBLNB}index=%-2s ====> ${CTBLNB}$namestring\n" $i
		fi
		
		let i++
	
	done

	until [[ $valid_index == "YES" ]]; do
	printf "${RESTORE}"
	printf "\n${CREQUEST}Enter desired numeric index value and press${CPROMPT}\n"
	read -p "[ENTER]:  " input_index

	case "${input_index^^}" in
		"QUIT"|"EXIT"   )
		 printf "${CALERT}%s${RESTORE}\n" "Exiting.  You entered $input_index."
		exit;;
	esac

	if  ! [[ ${input_index} =~ $ck_index ]]; then
		valid_index="NO"
		printf "${CWARN}Invalid entry\n"
	elif [[ "$count" -gt "$input_index" ]]; then
		valid_index="YES"
	fi

	done

else
	input_index=0
fi

return
