# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0	)
	printf "%s\nScript must be invoked via source command\nExiting\n"
	exit 1;;
esac

VERSION="1.4.0"
REGEN="5"
EXITIME="5"
FIGFONT="small"
SRC="/usr/libexec/ykocli"
BKGND="NO"
from_add_code="NO"
rq_touch="NO"
touch_em="NO"
action_req=$1
search_string=$2
start="1"
ck_index="^[0-9]+$"
device_index=""
ykman_table=$(mktemp)
CYKID="$LYELLOW"
CINFO="$GREEN"
CALERT="$LRED"
CERROR="$RED"
CBANNER="$BLUE"
CACTION="$PURPLE"
CHLITE="$LYELLOW"
CTBLNA="$YELLOW"
CTBLNB="$LBLUE"
CREQUEST="$BLUE"
CPROMPT="$LCYAN$BOLD"
CARROW="$LGRAY"
CWARN="$LPURPLE"

if [ -f "$HOME/.ykocli.conf" ]; then
	source "$SRC/src-yko-conf-override.sh"
fi

return
