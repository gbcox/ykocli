# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0	)
	printf "%s\nScript must be invoked via source command\nExiting\n"
	exit 1;;
esac

i=0
t=0
touch_count=0

mapfile -t touch_table < <(ykman -d $key_serial oath accounts code)
touch_count=${#touch_table[@]}

while [[ "$i" -lt "$touch_count" ]]; do
	fullstring=${touch_table[$i]}
	touchstring=$(echo -e $fullstring)
	namestring=${touch_table[$i]%% *}
	if [[ "$namestring" =~ "$search_string" ]] && [[ "$fullstring" =~ "Requires Touch" ]]; then
		input_index=$i
		touchname=$namestring
		touchfull=$fullstring
		let t++
	fi
	let i++
done

if [[ $t -eq 1 ]] && [[ $search_string != "" ]]; then
	touch_em="YES"
	namestring=$touchname
	fullstring=$touchfull
fi

return
