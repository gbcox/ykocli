# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0	)
	printf "%s\nScript must be invoked via source command\nExiting\n"
	exit 1;;
esac

shopt -s extglob

supported_actions="TOTP | RENAME | ADD | DELETE"
actions="@(BKGND|TOTP|RENAME|ADD|DELETE|QUIT|EXIT|--HELP|-H)"

source "$SRC/src-yko-figlet.sh"
until [[ $valid_action == "YES" ]]; do
	case "${action_req^^}" in
		$actions	)
			valid_action="YES";;
		*	)
		printf "${CREQUEST}Select Supported Action: ${CACTION}$supported_actions${CPROMPT}\n"
		read -p "[ENTER]: " action_req search_string
		printf "${RESTORE}\n";;
	esac
done

case "${action_req^^}" in
	"QUIT"|"EXIT"	)
	printf "${CALERT}%s${RESTORE}\n" "Exiting.  You entered $action_req."
	return 125;;
	"-H"|"--HELP"	)
	source "$SRC/src-yko-help.sh"
	return 125;;
	"BKGND"	)
	term_name=$(ps -o comm= -p "$(($(ps -o ppid= -p "$(($(ps -o sid= -p "$$")))")))")
	if [[ ${term_name^^} == "KONSOLE" ]] && [[ $XDG_CURRENT_DESKTOP == "KDE" ]]; then
		:
	else
		printf "${CALERT}%s${RESTORE}\n" "Detecting ${term_name^^} terminal running in $XDG_CURRENT_DESKTOP"
		printf "${CALERT}%s${RESTORE}\n" "Exiting. Background mode valid only with Konsole running under KDE Plasma."
		return 38
	fi
	search_string="";;
	"ADD"	)
	search_string="";;
	"RENAME"	)
	if [[ $firmware_level -ge 530 ]]; then
		:
	else
		printf "${CALERT}%s${RESTORE}\n" "$device_info"
		printf "${CALERT}%s${RESTORE}\n" "Exiting. Minimum Firmware 5.3.0 required for RENAME functionality."
		return 38
	fi
	action_msg="to ${CHLITE}${action_req^^} ";;
	"DELETE"	)
	action_msg="to ${CHLITE}${action_req^^} ";;
esac

ACTION="${action_req^^}"

shopt -u extglob

return
