package-name = ykocli
package-version = 1.0.2
INSTALL = /usr/bin/install -p

prefix = /usr/local
exec-prefix = $(prefix)
bindir = $(exec-prefix)/bin
libexecdir = $(prefix)/libexec/$(package-name)
sysconfdir = /etc
datarootdir = $(prefix)/share
docdir = $(datarootdir)/doc/$(package-name)
licensedir = $(datarootdir)/licenses/$(package-name)
mandir = $(datarootdir)/man
DESTDIR =

all:

clean:

install:
	$(INSTALL) -d -m 755 $(DESTDIR)$(bindir)
	$(INSTALL) -m 755 ykocli $(DESTDIR)$(bindir)
	$(INSTALL) -d -m 755 $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-set-colors.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-ck-input.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-figlet.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-help.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-table.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-time.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-trap.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-totp.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-add.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-delete.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-rename.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-devinfo.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-conf-override.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-ck-touch.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -m 644 src-yko-set-variables.sh $(DESTDIR)$(libexecdir)
	$(INSTALL) -d -m 755 $(DESTDIR)$(sysconfdir)
	$(INSTALL) -m 644 ykocli.conf $(DESTDIR)$(sysconfdir)
	$(INSTALL) -d -m 755 $(DESTDIR)$(licensedir)
	$(INSTALL) -m 644 LICENSE.md $(DESTDIR)$(licensedir)
	$(INSTALL) -d -m 755 $(DESTDIR)$(docdir)
	$(INSTALL) -m 644 README.md contributors.txt $(DESTDIR)$(docdir)
	$(INSTALL) -d -m 755 $(DESTDIR)$(docdir)/examples
	$(INSTALL) -m 644 ykocli.profile ykocli.desktop $(DESTDIR)$(docdir)/examples
	$(INSTALL) -m 644 ykocli-konsole-bkgnd.sh $(DESTDIR)$(docdir)/examples
	$(INSTALL) -d -m 755 $(DESTDIR)$(mandir)/man1
	$(INSTALL) -m 644 ykocli.1 $(DESTDIR)$(mandir)/man1

uninstall:

	-rm -v \
	$(DESTDIR)$(bindir)/ykocli \
	$(DESTDIR)$(libexecdir)/src-yko-set-colors.sh \
	$(DESTDIR)$(libexecdir)/src-yko-ck-input.sh \
	$(DESTDIR)$(libexecdir)/src-yko-help.sh \
	$(DESTDIR)$(libexecdir)/src-yko-table.sh \
	$(DESTDIR)$(libexecdir)/src-yko-time.sh \
	$(DESTDIR)$(libexecdir)/src-yko-trap.sh \
	$(DESTDIR)$(libexecdir)/src-yko-totp.sh \
	$(DESTDIR)$(libexecdir)/src-yko-add.sh \
	$(DESTDIR)$(libexecdir)/src-yko-delete.sh \
	$(DESTDIR)$(libexecdir)/src-yko-rename.sh \
	$(DESTDIR)$(libexecdir)/src-yko-figlet.sh \
	$(DESTDIR)$(libexecdir)/src-yko-devinfo.sh \
	$(DESTDIR)$(libexecdir)/src-yko-ck-touch.sh \
	$(DESTDIR)$(libexecdir)/src-yko-conf-override.sh \
	$(DESTDIR)$(libexecdir)/src-yko-set-variables.sh \
	$(DESTDIR)$(sysconfdir)/ykocli.conf \
	$(DESTDIR)$(licensedir)/LICENSE.md \
	$(DESTDIR)$(docdir)/README.md \
	$(DESTDIR)$(docdir)/contributors.txt \
	$(DESTDIR)$(docdir)/examples/ykocli.profile \
	$(DESTDIR)$(docdir)/examples/ykocli.desktop \
	$(DESTDIR)$(docdir)/examples/ykocli-konsole-bkgnd.sh \
	$(DESTDIR)$(mandir)/man1/ykocli.1
