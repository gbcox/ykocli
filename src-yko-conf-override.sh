# Copyright © 2023 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0	)
	printf "%s\nScript must be invoked via source command\nExiting\n"
	exit 1;;
esac

mapfile -t conf_table < "$HOME/.ykocli.conf"

for line in "${conf_table[@]}"; do
	line_action=${line%%=*}
	case $line_action in
	"REGEN"         )
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${line_var} =~ $ck_index ]]; then
			REGEN=$line_var
		fi;;
	"EXITIME"       )
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${line_var} =~ $ck_index ]]; then
			EXITIME=$line_var
		fi;;
	"FIGFONT"       )
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ -f "/usr/share/figlet/$line_var.flf" ]]; then
			FIGFONT=$line_var
		fi;;
	"CYKID"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CYKID=${term_color[$line_var]}
		fi;;
	"CINFO"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CINFO=${term_color[$line_var]}
		fi;;
	"CALERT"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CALERT=${term_color[$line_var]}
		fi;;
	"CERROR"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CERROR=${term_color[$line_var]}
		fi;;
	"CBANNER"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CBANNER=${term_color[$line_var]}
		fi;;
	"CACTION"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CACTION=${term_color[$line_var]}
		fi;;
	"CHLITE"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CHLITE=${term_color[$line_var]}
		fi;;
	"CTBLNA"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CTBLNA=${term_color[$line_var]}
		fi;;
	"CTBLNB"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CTBLNB=${term_color[$line_var]}
		fi;;
	"CREQUEST"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CREQUEST=${term_color[$line_var]}
		fi;;
	"CPROMPT"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CPROMPT=${term_color[$line_var]}
		fi;;
	"CARROW"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CARROW=${term_color[$line_var]}
		fi;;
	"CWARN"			)
		line_var=${line##*=}
		line_var=${line_var%\"}
		line_var=${line_var#\"}
		if [[ ${term_color[$line_var]} ]]; then
			CWARN=${term_color[$line_var]}
		fi;;
	esac
done

return
