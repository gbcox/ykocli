# Copyright © 2022 Gerald B. Cox
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case $BASH_SOURCE in
	$0	)
	printf "%s\nScript must be invoked via source command\nExiting\n"
	exit 1;;
esac

fullstring=${MAPFILE[$input_index]}
namestring=${MAPFILE[$input_index]% *}
namestring="${namestring%"${namestring##*[![:space:]]}"}"

if [[ $namestring =~ ":" ]]; then
	issuer=${namestring%:*}
	demarc=":"
	account_name=${namestring#*:}
else
	issuer=""
	demarc=""
	account_name=$namestring
fi

#issuer limited to 51
#account name limited to 54
printf "\n${CREQUEST}RENAME ${CARROW}===> ${CHLITE}$namestring ${CARROW}<===\n"
printf "${CINFO}This will change how the account is displayed in the list.${RESTORE}\n"

if [[ $issuer != "" ]]; then
	printf "${CPROMPT}"
	read -p "[ENTER] Issuer to replace $issuer: " rename_issuer
	printf "${RESTORE}"
fi

printf "${CPROMPT}"
read -p "[ENTER] Account Name to replace $account_name: " rename_account
printf "${RESTORE}"

if [[ $rename_issuer == "" ]]; then
	rename_issuer=$issuer
fi

if [[ $rename_account == "" ]]; then
	rename_account=$account_name
fi

rename_namestring=$rename_issuer$demarc$rename_account

if [[ $rename_namestring == $namestring ]]; then
	printf "\n${CALERT}No change detected.  ${CALERT}Exiting.${RESTORE}\n"
	return 125
else
	printf "\n${CACTION}RENAME\n"
	printf "${CARROW}===> ${CHLITE}$namestring ${CARROW}<=== ${CINFO}to\n"
	printf "${CARROW}===> ${CHLITE}$rename_namestring ${CARROW}<=== ${CINFO}?\n"
	printf "${CREQUEST}You must reply ${CHLITE}YES${CREQUEST} to confirm.${CPROMPT}\n"
	read -p "[ENTER] YES to confirm: " rename_confirmation
	if [[ $rename_confirmation == "YES" ]]; then
		rename_code=$(ykman -d $key_serial oath accounts rename --force "$namestring" "$rename_namestring")
		printf "${CINFO}$rename_code ${CALERT}EXITING.${RESTORE}\n"
	else
		printf "${CINFO}Valid confirmation not received.  ${CALERT}EXITING.${RESTORE}\n"
		return 125
	fi
fi

return
